import React, { Component } from 'react'
import { Route, NativeRouter, Switch, Redirect } from 'react-router-native'
import { Provider } from 'react-redux'
import store from './AppStore';
import Login from './Login'
import List from './List'
import Profile from './Profile'
import EditProfile from './EditProfile'
import Product from './Product'
import AddProduct from './AddProduct'
import EditProduct from './EditProduct'


class Router extends Component {
    render() {
        return (
            <Provider store={store}>
                <NativeRouter>
                    <Switch>
                        <Route path="/login" component={Login} />
                        <Route path="/list" component={List} />
                        <Route path="/profile" component={Profile} />
                        <Route path="/editprofile" component={EditProfile} />
                        <Route path="/product" component={Product} />
                        <Route path="/addproduct" component={AddProduct} />
                        <Route path="/editproduct" component={EditProduct} />
                        <Redirect to="/login" />
                    </Switch>
                </NativeRouter>
            </Provider>
        )
    }
}

export default Router