export default (state = [], action) => {
    switch (action.type) {
        case 'ADD_USER':
            return [...state, {
                username: action.username,
                firstname: '',
                lastname: ''
            }]
        case 'EDIT_USER':
            return state.map((each, index) => {
                if (each.username === action.username) {
                    return {
                        ...each,
                        firstname: action.firstname,
                        lastname: action.lastname
                    }
                }
                return each
            })

        default:
            return state
    }
}