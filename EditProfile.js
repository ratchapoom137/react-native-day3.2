/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Alert, TouchableOpacity, TextInput, Image, Modal, TouchableHighlight } from 'react-native';
import { Button, WhiteSpace, WingBlank, InputItem, Icon } from '@ant-design/react-native';
import { connect } from 'react-redux'


const instructions = Platform.select({
    ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
    android:
        'Double tap R on your keyboard to reload,\n' +
        'Shake or press menu button for dev menu',
});

class EditProfile extends Component {

    state = {
        visible: false,
        firstname: '',
        lastname: ''
    };

    UNSAFE_componentWillMount() {
        console.log(this.props)
    }

    showModal = () => {
        this.setState({
            visible: true,
        });
    }

    handleOk = (e) => {
        console.log(e);
        this.setState({
            visible: false,
        });
    }

    handleCancel = (e) => {
        console.log(e);
        this.setState({
            visible: false,
        });
    }

    goBack = () => {
        this.props.history.push('/profile')
    }

    save = () => {
        this.props.addTodo(this.props.todos[0].username, this.state.firstname, this.state.lastname);
        this.props.history.push('/profile')
    }

    render() {
        const { todos, addTodo } = this.props
        console.log(todos)
        return (
            <View style={styles.container}>
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={this.state.visible}
                    onRequestClose={() => {
                        Alert.alert('Modal has been closed.');
                        this.handleCancel;
                    }}
                >
                    <TouchableOpacity style={styles.modal} onPress={() => { this.handleCancel(); }}>
                        <Text style={styles.textModal}>Close</Text>
                    </TouchableOpacity>
                </Modal>
                <View style={styles.header}>
                    <Button style={{ width: 60, height: 40 }} size="small" type="primary" onPress={() => { this.goBack(); }} >{'<'}</Button>
                    <View style={styles.box2}>
                        <Text style={styles.text}>Edit Profile</Text>
                    </View>
                </View>
                <View style={styles.boxContent}>
                    <InputItem
                        clear
                        value={this.state.firstname}
                        onChange={firstname => {
                            this.setState({
                                firstname,
                            });
                        }}
                        placeholder="First name"
                    >
                        <Icon name="edit" size="md" color="black" />
                    </InputItem>
                    <InputItem
                        clear
                        value={this.state.lastname}
                        onChange={lastname => {
                            this.setState({
                                lastname,
                            });
                        }}
                        placeholder="Last name"
                    >
                        <Icon name="edit" size="md" color="black" />
                    </InputItem>
                </View>
                <View style={styles.box1}>
                    <Button style={{ width: 200 }} type="primary" onPress={() => { this.save() }} >Save</Button>
                </View>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // justifyContent: 'center',
        // alignItems: 'center',
        backgroundColor: '#00ffcc',
    },
    header: {
        backgroundColor: "#00ffcc",
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "center",
        marginHorizontal: -8
    },

    headerText: {
        color: "black",
        fontSize: 30,
        fontWeight: "bold",
    },

    content: {
        backgroundColor: "#00ffcc",
        flexDirection: "row",
        flex: 1
        // alignItems: "center",
        // justifyContent: "center",
    },

    image: {
        width: 150,
        height: 150,
        borderRadius: 40,
        margin: 5
    },

    box1: {
        margin: 50,
        alignItems: 'center',
        justifyContent: 'center'
    },

    box2: {
        backgroundColor: "green",
        padding: 10,

        flex: 5,
        alignItems: 'center',
        justifyContent: 'center'
    },

    boxContent: {
        backgroundColor: "#00ffcc",
        margin: 3,
        flex: 1
    },

    boxProfile: {
        margin: 10,
    },

    modal: {
        position: 'absolute',
        top: 20,
        right: 20,
        left: 20,
        bottom: 20,
        backgroundColor: 'rgba(0,0,0,0.6)',
        justifyContent: 'center',
        alignItems: 'center'
    },

    textModal: {
        color: 'white',
        fontSize: 20
    },

    textTitle: {
        color: 'black',
        fontSize: 25,
        fontWeight: "bold",
    }

});

const mapStateToProps = (state) => {
    return {
        todos: state.todos
    }
}

const mapDidpatchToProps = (dispatch) => {
    return {
        addTodo: (username, firstname, lastname) => {
            dispatch({
                type: 'EDIT_USER',
                username: username,
                firstname: firstname,
                lastname: lastname
            })
        }
    }
}

export default connect(mapStateToProps, mapDidpatchToProps)(EditProfile)