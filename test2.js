const todoReducer = (state = [], action) => {
    switch (action.type) {
        case 'ADD_TODOS' :
            return [...state, {
                topic: action.topic,
                completed: false
            }]
        case 'TOGGLE_TODOS':
            return state.map((each, index) => {
                if (index === action.targetIndex) {
                    return {
                        ...each,
                        completed: !each.completed
                    }
                }
                return each
            })
        case 'REMOVE_TODOS' :
            return state.filter((each, index) => {
                return index !== action.targetIndex
            })
        default:
            return state
    }
}
 
const visibilityReducer = (state = "", action) => {
    switch (action.type) {
        case 'CHANGE_ALL':
            return "all"
        case 'CHANGE_COMPLETE':
            return "complete"
        case 'CHANGE_HIDE':
            return "hide"
        default:
            return "hide"
    }
}
 
const combineReducer = (reducers = {}) => {
    return (state, action) => Object.keys(reducers).reduce((sum, key)=>{
        sum[key] = reducers[key](state[key], action)
        return sum
    }, {})
}
 
const createStore = reducer => {
 
    let state = {}
 
   
    getState = () => state
 
    dispatch = action => {
        state = reducer(state, action)
        listeners.forEach(fn => {
            fn()
        })
    }
 
    let listeners = []
 
    subscribe = (fn) => {
        listeners.push(fn)
        return () => {
            listeners = listeners.filter(each => {
                return each !== fn
            })
        }
    }
 
    dispatch({ type: 'INIT' })
 
    return {
        getState,
        dispatch,
        subscribe
    }
}
 
const reducers = combineReducer({
    todos: todoReducer,
    visibility: visibilityReducer
})
const store = createStore(reducers)
const unsubTodo = store.subscribe(() => {console.log(store.getState())})
 
store.dispatch({
    type: 'ADD_TODOS',
    topic: 'เอาผ้าไปซัก'
})
 
store.dispatch({
    type: 'ADD_TODOS',
    topic: 'รดน้ำต้นไม้'
})
 
store.dispatch({
    type: 'ADD_TODOS',
    topic: 'ซื้อพิซซ่า'
})
 
store.dispatch({
    type: 'CHANGE_ALL'
})
 
store.dispatch({
    type: 'TOGGLE_TODOS',
    targetIndex: 1
})
 
store.dispatch({
    type: 'TOGGLE_TODOS',
    targetIndex: 1
})
 
 
store.dispatch({
    type: 'REMOVE_TODOS',
    targetIndex: 1
})
 
store.dispatch({
    type: 'CHANGE_COMPLETE'
})