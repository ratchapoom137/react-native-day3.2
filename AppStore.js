import { createStore, combineReducers } from 'redux';

import SaveInfoUsers from './SaveInfoUsers'
import SaveInfoProduct from './SaveInfoProduct'

const reducers = combineReducers({
    todos: SaveInfoUsers,
    product: SaveInfoProduct
})

const store = createStore(reducers);
const state = store.getState();
console.log(state)
export default store