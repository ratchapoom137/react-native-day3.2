/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Alert, TouchableOpacity, TextInput, Image, Modal, ScrollView } from 'react-native';
import { Button, WhiteSpace, WingBlank, List, InputItem, Icon } from '@ant-design/react-native';
import { connect } from 'react-redux'
import AddProduct from './AddProduct';

const instructions = Platform.select({
    ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
    android:
        'Double tap R on your keyboard to reload,\n' +
        'Shake or press menu button for dev menu',
});

class Login extends Component {

    state = {
        visible: false,
        username: '',
        password: '',
        firstname: '',
        lastname: '',
        status: '',
        // user: {
        //     username: '',
        //     password: ''
        // }
    };

    UNSAFE_componentWillMount() {
        console.log(this.props)
    }

    // constructor(props) {
    //   super(props);
    //   this.state = { username: '' };ss
    // }

    onClickLogin = () => {
        this.props.addTodo(this.state.username);
        this.props.AddProduct('','');
        this.props.history.push('/list')
    }

    handleCancel = (e) => {
        console.log(e);
        this.setState({ visible: false })
    }

    render() {
        const { todos, addTodo, AddProduct } = this.props
        console.log(todos)
        return (
            <View style={styles.container}>
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={this.state.visible}
                    onRequestClose={() => {
                        Alert.alert('Modal has been closed.');
                        this.handleCancel();
                    }}
                >
                    <TouchableOpacity style={styles.modal} onPress={() => { this.handleCancel(); }}>
                        <Text style={styles.textModal}>{this.state.status}</Text>
                    </TouchableOpacity>
                </Modal>
                <View style={styles.header}>
                    <View style={styles.image}>
                        <Image
                            style={{ width: 200, height: 200, borderRadius: 100 }}
                            source={{ uri: 'https://sv1.picz.in.th/images/2019/02/12/ToPrc9.jpg' }}
                        />
                    </View>
                </View>
                <ScrollView
                    style={{ flex: 1 }}
                    automaticallyAdjustContentInsets={false}
                    showsHorizontalScrollIndicator={false}
                    showsVerticalScrollIndicator={false}
                >
                    <View style={styles.content}>
                        <View style={{ alignItems: 'center' }}>
                            <Text >{this.state.username} - {this.state.password}</Text>
                        </View>
                        <View style={styles.box1}>
                            {/* <TextInput style={styles.text} placeholder="Username" onChangeText={value => this.setState({ username: value })} /> */}
                            <InputItem
                                clear
                                value={this.state.username}
                                onChange={username => {
                                    this.setState({
                                        username,
                                    });
                                }}
                                placeholder="username"
                            >
                                <Icon name="user" size="md" color="black" />
                            </InputItem>
                        </View>
                        <View style={styles.box1}>
                            {/* <TextInput style={styles.text} tyle="password" placeholder="Password" onChangeText={value => this.setState({ password: value })} /> */}
                            <InputItem
                                clear
                                value={this.state.password}
                                onChange={password => {
                                    this.setState({
                                        password,
                                    });
                                }}
                                placeholder="password"
                            >
                                <Icon name="lock" size="md" color="black" />
                            </InputItem>
                        </View>

                        <View style={styles.box2}>
                            {/* <Button title="Login" onPress={() => { this.onClickLogin(); }} /> */}
                            <Button type="primary" onPress={() => { this.onClickLogin() }} >Login</Button>
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // justifyContent: 'center',
        // alignItems: 'center',
        backgroundColor: '#66ccff',
    },
    header: {
        backgroundColor: "#66ccff",
        alignItems: "center",
        justifyContent: "center",
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    image: {
        backgroundColor: "white",
        alignItems: 'center',
        justifyContent: 'center',
        width: 200,
        height: 200,
        borderRadius: 100
    },
    headerText: {
        color: "black",
        fontSize: 30,
        fontWeight: "bold",
    },

    content: {
        backgroundColor: "#66ccff",
        flex: 1,
        flexDirection: "column",
    },

    box1: {
        backgroundColor: "white",
        margin: 3,
    },

    box2: {
        margin: 50,
    },

    row: {
        backgroundColor: "#66ccff",
        padding: 5,
        margin: 8,
        flexDirection: "row",
        // alignItems: 'center',
        // justifyContent: 'center'
    },

    modal: {
        position: 'absolute',
        top: 220,
        right: 20,
        left: 20,
        bottom: 220,
        backgroundColor: 'rgba(0,0,0,0.8)',
        justifyContent: 'center',
        alignItems: 'center'
    },

    textModal: {
        color: 'white',
        fontSize: 20
    },

    ButtonX: {
        alignItems: 'center',
        justifyContent: 'flex-end',
        width: 50
    }
});

// const mapStateToProps = (state) => {
//     return {
//         Info: state.SaveInfo
//     }
// }

// const mapDidpatchToProps = (dispatch) => {
//     return {
//         addInfo: (username, password, firstname, lastname) => {
//             dispatch({
//                 type: 'ADD_INFO',
//                 username: username,
//                 password: password,
//                 firstname: firstname,
//                 lastname: lastname
//             })
//         }
//     }
// }

const mapStateToProps = (state) => {
    return {
        todos: state.todos
    }
}

const mapDidpatchToProps = (dispatch) => {
    return {
        addTodo: (username) => {
            dispatch({
                type: 'ADD_USER',
                username: username
            })
        },
        AddProduct: (image, imagename) => {
            dispatch({
                type: 'ADD_PRODUCT',
                image: image,
                imagename: imagename
            })
        }
    }
}

export default connect(mapStateToProps, mapDidpatchToProps)(Login)