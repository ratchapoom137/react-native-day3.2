export default (state = [], action) => {
    switch (action.type) {
        case 'ADD_PRODUCT':
            return [...state, {
                image: action.image,
                imagename: action.imagename
            }]
        case 'EDIT_PRODUCT':
            return state.map((each, index) => {
                if (each.imagename === action.imagename) {
                    return {
                        ...each,
                        image: action.firstname,
                        lastname: action.lastname
                    }
                }
                return each
            })

        default:
            return state
    }
}