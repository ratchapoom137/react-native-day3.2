/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Alert, TouchableOpacity, TextInput, Image, Modal, TouchableHighlight } from 'react-native';
import { Button, WhiteSpace, WingBlank } from '@ant-design/react-native';
import { connect } from 'react-redux'

const instructions = Platform.select({
    ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
    android:
        'Double tap R on your keyboard to reload,\n' +
        'Shake or press menu button for dev menu',
});

class List extends Component {

    state = {
        visible: false,
        products: []
    };
    UNSAFE_componentWillMount() {
        console.log(this.props)
    }

    showModal = () => {
        this.setState({
            visible: true,
        });
    }

    handleOk = (e) => {
        console.log(e);
        this.setState({
            visible: false,
        });
    }

    handleCancel = (e) => {
        console.log(e);
        this.setState({
            visible: false,
        });
    }

    goToLogin = () => {
        this.props.history.push('/login')
    }

    goToAddproductPage = () => {
        this.props.history.push('/addproduct')
    }

    goToProfilePage = () => {
        this.props.history.push('/profile')
    }

    gotoProductPage = () => {
        this.props.history.push('/product')
    }

    showProduct = () => {
        for (let i = 0; i < this.props.product.length; i++) {
            this.state.products.push(
                <View style={styles.boxImage}>
                    <Text style={styles.textTitle}>{this.props.product[i].image}</Text>
                    <Text style={styles.textTitle}>{this.props.product[i].imagename}</Text>
                </View>
            )
        }
    }

    render() {
        const { product } = this.props
        console.log(product)
        this.showProduct()
        return (
            <View style={styles.container}>
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={this.state.visible}
                    onRequestClose={() => {
                        Alert.alert('Modal has been closed.');
                        this.handleCancel;
                    }}
                >
                    <TouchableOpacity style={styles.modal} onPress={() => { this.handleCancel(); }}>
                        <Text style={styles.textModal}>Close</Text>
                    </TouchableOpacity>
                </Modal>
                <View style={styles.header}>
                    <View style={styles.box2}>
                        <Text style={styles.text}>List</Text>
                    </View>
                </View>
                <View style={styles.MainContent}>
                    <View style={styles.content}>
                        {/* <Image
                                style={styles.image}
                                source={{ uri: 'https://sv1.picz.in.th/images/2019/02/12/ToPrc9.jpg' }}
                            /> */}
                        {/* <Text style={styles.textTitle}>{product[0].image}</Text>
                            <Text style={styles.textTitle}>{product[0].imagename}</Text> */}
                        {this.state.products}
                    </View>

                </View>
                <View style={styles.header}>
                    <Button style={{ flex: 1, height: 40 }} size="small" type="primary" onPress={() => { this.goToLogin(); }}>L</Button>
                    <Button style={{ flex: 2, height: 40 }} size="small" type="primary" onPress={() => { this.goToAddproductPage(); }}>Add Product</Button>
                    <Button style={{ flex: 1, height: 40 }} size="small" type="primary" onPress={() => { this.goToProfilePage(); }}>P</Button>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // justifyContent: 'center',
        // alignItems: 'center',
        backgroundColor: '#00ffcc',
    },
    header: {
        backgroundColor: "#00ffcc",
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "center",
        marginHorizontal: -8
    },

    headerText: {
        color: "black",
        fontSize: 30,
        fontWeight: "bold",
    },

    MainContent: {
        backgroundColor: '#00ffcc',
        flex: 1,
        // alignItems: "center",
        // justifyContent: "center"
    },

    content: {
        backgroundColor: "#00ffcc",
        flexDirection: "row",
        // alignItems: "center",
        // justifyContent: "center",
    },

    image: {
        width: 140,
        height: 140,
        borderRadius: 40,
        margin: 5
    },

    box1: {
        backgroundColor: "green",
        padding: 10,
        margin: 2,
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },

    box2: {
        backgroundColor: "green",
        padding: 10,
        flex: 5,
        alignItems: 'center',
        justifyContent: 'center'
    },

    boxImage: {
        alignItems: "center",
        justifyContent: "center",
    },

    modal: {
        position: 'absolute',
        top: 20,
        right: 20,
        left: 20,
        bottom: 20,
        backgroundColor: 'rgba(0,0,0,0.6)',
        justifyContent: 'center',
        alignItems: 'center'
    },

    textModal: {
        color: 'white',
        fontSize: 20
    },

    textTitle: {
        color: 'black'
    }

});


const mapStateToProps = (state) => {
    return {
        product: state.product
    }
}

export default connect(mapStateToProps)(List)